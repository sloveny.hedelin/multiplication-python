"""
R0
Demander une table
Poser question
Renvoyer observation
Donner nombre de bonnes reponses
Renvoyer commentaires sur le resultat


R1
Demander une table
    Demander au user un nombre
Poser question aleatoires
    Poser 10 questions pour le nombre demandé
Renvoyer observation
    Dire Correct ou Erreur
Donner nombre de bonnes reponses
    Afficher un compteur
Renvoyer commentaires sur le resultat
    Afficher un commentaire en fonction des bonnes reponses

R2
Demander une table 
    Demander au user un nombre
        Permettre au user de rentrer un nombre entier
Poser question aleatoires (comment?)
    Poser 10 questions pour le nombre demandé
        Tant que 10 questions ne sont pas posées, ne pas s'arreter.
    Choisir un nombre aléatoire entre 1 et 10
        Poser nombre proposé par user multiplié par 
        nombre aleatoire entre 1 et 10
    Lancer chrono pour calculer le temps de réponse
Renvoyer observation - (Comment?)
    Arreter chrono
    Calculer le temps de réponse
    creer un compteur pour le calcul des multiplications
    Créer compteur bonnes reponses
        Dire Correct ou Erreur

            Si reponse correcte, renvoyer Correct
            Incrementer le compteur bonnes réponses.
            Sinon renvoyer Erreur
Donner nombre de bonnes reponses
    Afficher un compteur
Renvoyer commentaires sur le resultat
    Afficher un commentaire en fonction des bonnes reponses
Renvoyer le temps
    Renvoyer le temps du chrono et la moyenne du temps des réponses
Proposer de quitter ou continuer

Si 10 bonnes réponses, afficher 'Excellent'
« Excellent !» si toutes les réponses sont justes,
— « Très bien. » s’il n’a commis qu’une seule erreur,
— « Bien. » s’il n’a pas fait plus de trois erreurs,
— « Moyen. » s’il a 4, 5 ou 6 bonnes réponses,
— « Il faut retravailler cette table. » s’il a 3 bonnes réponses ou moins et
— « Est-ce que tu l’as fait exprès ?" si toutes les réponses sont fausses


Scenario :
Le programme affiche :  
    Quel table souhaitez vous reviser?
    Le user entre le chiffre 1
    Le programme propose aléatoirement
    Le chrono du temps de réponse est lancé
        1X1 =
    Le user donne une réponse = 1
    Le chrono s'arrete
    Le programme affiche : Correct

    Le programme propose ainsi 9 autres opérations au user pour la table de 1
    A chaque réponse, le programme affiche soit Correct ou Erreur.
    Au bout de 10, le prgramme affiche le nombre de bonnes réponses.
    Le programme affiche un message en fonction du nombre de bonnes reponses.
    Le programme propose au user soit de quitter ou de continuer.
