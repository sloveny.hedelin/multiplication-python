from random import randint, sample
from time import time
import math


def table_multiplication() -> int:
    terminer = "o"
    compteurs_reponses = 0
    while terminer == "o":
        try:
            nombre_proposer = int(input("Quelle table souhaitez vous réviser ? "))
            multiplicateur = sample(list(range(1,11)), 10)
            counter = 0
            chrono = 0
            moyenne = 0
            while counter < 10:
                try:
                    start: float = time()
                    la_reponse = int(input(f"{nombre_proposer} * {multiplicateur[counter]} = "))
                    end: float = time()
                    chrono = float(end - start) if float(end - start) >= chrono else chrono
                    moyenne += float(end - start)
                    calcul = multiplicateur[counter] * nombre_proposer
                    counter += 1
                    if calcul == la_reponse:
                        compteurs_reponses+=1
                        print("Correct")
                    else:
                        print("Erreur")
                except ValueError:
                    pass
                    # print()

        except ValueError:
             pass
            # print('')


        if compteurs_reponses==10:
            print("Excellent !")
        if compteurs_reponses ==9:
            print("Très bien")
        if compteurs_reponses >=7 and compteurs_reponses<= 8:
            print("Bien")
        if compteurs_reponses>=4 and compteurs_reponses<=6:
            print("Moyen")
        if compteurs_reponses>=1 and compteurs_reponses<3:
            print("Il faut retravailler cette table")
        else:
            print("Tu l'as fait exprès ?")
        moyenne /= 10
        print("Vous avez ", compteurs_reponses, "bonnes réponses !")
        print("Votre temps de réponse le plus long est de", round(chrono), "secondes")
        print("Votre moyenne de temps de réponse est de :", round(moyenne, 2), "secondes")
        terminer = input("Voulez-vous recommencer ? o/n: ").lower()


table_multiplication()


"""
Raffinage

Demander une table (comment?)
    Demander au user un nombre
    Permettre au user de rentrer un nombre entier
    int(input)
    creer un compteur pour stocker les résultats attendus des multiplications
    Créer compteur bonnes reponses pour stocker le nombre de bonnes réponses
Poser question aleatoire (comment?)
    Poser 10 questions pour la table demandée par user
        Tant que 10 questions ne sont pas posées, ne pas s'arreter.
    Choisir un nombre aléatoire entre 1 et 10 en tant que ultiplicateur
        Poser nombre proposé par user multiplié par nombre aleatoire entre 1 et 10
    Attendre réponse de user
    Lancer chrono pour calculer le temps de réponse
Renvoyer indicateurs- (Comment?)
    Arreter chrono
    Si réponse est bonne renvoyer Correct
    Incrementer le compteur bonnes réponses.
    Sinon renvoyer Erreur
Afficher message avec le nombre de bonnes reponses et un commentaire en fonction des bonnes reponses
Renvoyer le temps du chrono et la moyenne du temps des réponses
Proposer de quitter ou continuer

Scenario brouillon:
Le programme affiche :  
    Quel table souhaitez vous reviser?
    Le user entre le chiffre 1
    Le programme propose une question avec un multiplicateur aléatoire entre 1 et 10:
    {Le chrono du temps de réponse est lancé}
    1X1 =
    Le user répond = 1
    {Le chrono s'arrete}
    Le programme affiche : 
     Correct
    Le programme repose une question:
    1X3 =
    Le user répond = 1
    Le programme affiche:
    Erreur
    ...Ainsi pendant 10 fois
    Après 10 questions :
    le programme affiche un commentaire sur le nombre de bonnes réponses, le temps le plus long pour une réponse et la moyenne de temps de réponses.
    Le programme propose de continuer ou quitter"""